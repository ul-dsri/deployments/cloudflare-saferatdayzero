# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "cloudflare_zone" "saferatdayzero_dev" {
  account_id = local.cloudflare_account_id
  zone       = "saferatdayzero.dev"
}

resource "cloudflare_zone_settings_override" "saferatdayzero_dev" {
  zone_id = cloudflare_zone.saferatdayzero_dev.id
  settings {
    always_use_https         = "on"
    automatic_https_rewrites = "on"
    brotli                   = "on"
    browser_check            = "on"
    cache_level              = "aggressive"
    development_mode         = "off"
    early_hints              = "on"
    email_obfuscation        = "on"
    min_tls_version          = "1.2"
    opportunistic_encryption = "on"
    rocket_loader            = "on"
    security_level           = "medium"
    ssl                      = "strict"
    tls_1_3                  = "on"
    universal_ssl            = "on"

    minify {
      css  = "on"
      js   = "on"
      html = "on"
    }

    security_header {
      enabled = true
    }
  }
}

# domain

# It's normally not possible to have a CNAME for an apex domain, but Cloudflare
# supports CNAME flattening for this purpose:
# https://developers.cloudflare.com/dns/cname-flattening/
resource "cloudflare_record" "saferatdayzero_dev_cname" {
  zone_id = cloudflare_zone.saferatdayzero_dev.id
  type    = "CNAME"
  name    = "saferatdayzero.dev"
  value   = "saferatdayzero.gitlab.io"
  proxied = true
}

# domain verification

resource "cloudflare_record" "saferatdayzero_dev_gitlab" {
  zone_id = cloudflare_zone.saferatdayzero_dev.id
  type    = "TXT"
  name    = "_gitlab-pages-verification-code"
  value   = "gitlab-pages-verification-code=${gitlab_pages_domain.saferatdayzero_dev.verification_code}"
}

resource "cloudflare_record" "saferatdayzero_dev_google" {
  zone_id = cloudflare_zone.saferatdayzero_dev.id
  type    = "TXT"
  name    = "saferatdayzero.dev"
  value   = "google-site-verification=2jQ0A7XaLCAIF8TT3eZavlPicCETwmSGBL5RSLN4rAI" # pragma: allowlist secret
}

resource "cloudflare_record" "saferatdayzero_dev_bing" {
  zone_id = cloudflare_zone.saferatdayzero_dev.id
  type    = "CNAME"
  name    = "79b9e7eaf62de096adefb4bd3027acf4" # pragma: allowlist secret
  value   = "verify.bing.com"
}

# email security

resource "cloudflare_record" "saferatdayzero_dev_spf" {
  zone_id = cloudflare_zone.saferatdayzero_dev.id
  type    = "TXT"
  name    = "saferatdayzero.dev"
  value   = "v=spf1 -all"
}

resource "cloudflare_record" "saferatdayzero_dev_dmarc" {
  zone_id = cloudflare_zone.saferatdayzero_dev.id
  type    = "TXT"
  name    = "_dmarc"
  value   = "v=DMARC1; p=reject; sp=reject; adkim=s; aspf=s;"
}

resource "cloudflare_record" "saferatdayzero_dev_dkim" {
  zone_id = cloudflare_zone.saferatdayzero_dev.id
  type    = "TXT"
  name    = "*._domainkey"
  value   = "v=DKIM1; p="
}
