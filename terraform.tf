# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4.30.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 16.11.0"
    }
  }
  required_version = ">=1.0"
}
