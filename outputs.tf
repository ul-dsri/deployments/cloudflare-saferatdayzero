# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

output "name_servers" {
  value = join("\n", cloudflare_zone.saferatdayzero_dev.name_servers)
}
