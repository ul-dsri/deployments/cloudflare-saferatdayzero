# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  deployment = "cloudflare-zone-saferatdayzero"
  name       = "${var.environment}-${local.deployment}"

  default_tags = {
    deployment  = local.deployment
    environment = var.environment
  }

  cloudflare_account_id = "c71f34685220d85ddd7a0e531ba2635d"
}
