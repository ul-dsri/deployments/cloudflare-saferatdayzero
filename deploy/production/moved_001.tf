# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

moved {
  from = cloudflare_record.saferatdayzero_dev_bing
  to   = module.root.cloudflare_record.saferatdayzero_dev_bing
}
moved {
  from = cloudflare_record.saferatdayzero_dev_cname
  to   = module.root.cloudflare_record.saferatdayzero_dev_cname
}
moved {
  from = cloudflare_record.saferatdayzero_dev_dkim
  to   = module.root.cloudflare_record.saferatdayzero_dev_dkim
}
moved {
  from = cloudflare_record.saferatdayzero_dev_dmarc
  to   = module.root.cloudflare_record.saferatdayzero_dev_dmarc
}
moved {
  from = cloudflare_record.saferatdayzero_dev_gitlab
  to   = module.root.cloudflare_record.saferatdayzero_dev_gitlab
}
moved {
  from = cloudflare_record.saferatdayzero_dev_google
  to   = module.root.cloudflare_record.saferatdayzero_dev_google
}
moved {
  from = cloudflare_record.saferatdayzero_dev_spf
  to   = module.root.cloudflare_record.saferatdayzero_dev_spf
}
moved {
  from = cloudflare_zone.saferatdayzero_dev
  to   = module.root.cloudflare_zone.saferatdayzero_dev
}
moved {
  from = cloudflare_zone_settings_override.saferatdayzero_dev
  to   = module.root.cloudflare_zone_settings_override.saferatdayzero_dev
}
moved {
  from = gitlab_pages_domain.saferatdayzero_dev
  to   = module.root.gitlab_pages_domain.saferatdayzero_dev
}
