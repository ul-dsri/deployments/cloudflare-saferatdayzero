# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "gitlab_pages_domain" "saferatdayzero_dev" {
  project = 57239019 # https://gitlab.com/saferatdayzero/docs
  domain  = "saferatdayzero.dev"

  auto_ssl_enabled = true
}
